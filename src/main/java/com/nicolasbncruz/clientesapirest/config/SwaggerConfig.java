package com.nicolasbncruz.clientesapirest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()//Docket usa select() para obtener un objeto generador de archivos de la clase ApiSelectorBuilder
                .paths(PathSelectors.ant("/api/**"))
                .apis(RequestHandlerSelectors.basePackage("com.nicolasbncruz.clientesapirest"))
                .build()//llama al método build() para obtener el objeto preparado de un generador de archivos.
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Clientes Rest API",//titulo
                "Sirve para dar mantenimiento a los clientes del API.",//desripción
                "Version 1.0",
                "Terminos de servicio",//Url de términos de servicio
                new Contact("Nicolas Cruz", "http://www.nicolasbncruz.com", "nicolasbncruz@hotmail.com"),
                "Licencia de API", "http://www.licence.clientesapirest.com", Collections.emptyList());
    }
}
