package com.nicolasbncruz.clientesapirest.models.dao;

import com.nicolasbncruz.clientesapirest.models.entity.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface IClienteDao extends CrudRepository<Cliente, Long> {
}
